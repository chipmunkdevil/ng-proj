import { Component , OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'httpclient01';
  sources : Object;  
  sources_list : Object;  
  myDate: Date;

  constructor(private http: HttpClient) {

  }


  ngOnInit(): void {        
    // this.http.get('http://37.140.197.121:8000/status-json.xsl').subscribe(res =>{      
    //   this.sources = res.icestats.source;      
    //   console.log(res.icestats.source.length);
    //     for (var isource in res.icestats.source)  {
    //       console.log(res.icestats.source[isource].title);
    //       console.log(res.icestats.source[isource].server_url);
    //       console.log(res.icestats.source[isource].genre);
    //     }   
    //  })        
     this.utcTime();
  }

  utcTime(): void {
    setInterval(() => {         //replaced function() by ()=>
      this.http.get('http://37.140.197.121:8000/status-json.xsl').subscribe((res:any) =>{                 
      this.sources = res.icestats.source;
      // console.log(res.icestats.source.length);
      //   for (var isource in res.icestats.source)  {
      //     console.log(res.icestats.source[isource].title);
      //     console.log(res.icestats.source[isource].server_url);
      //     console.log(res.icestats.source[isource].genre);
      //   }   
     })        
      this.myDate = new Date();
      console.log(this.myDate); // just testing if it is working
    }, 5000);
}

}
